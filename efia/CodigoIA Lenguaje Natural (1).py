import nltk
from nltk import CFG
from nltk.parse.generate import generate
Gramatical = CFG.fromstring("""
    S ->NP VP
    NP -> DT N | N | NP PP
    VP -> V NP | V | VP PP
    PP -> P NP | P
    DT -> 'el' | 'los'
    N  -> 'hombre' | 'amigos' | 'cafe' | 'leche'
    V  -> 'toma' | 'toman'
    P  -> 'con' | 'solo'
    """)
print('La gramatica: ', Gramatical)
print('Inicio =>', Gramatical.start())
print('Producciones ->')
print(Gramatical.productions())
try:
    Gramatical.check_coverage('el','hombre','toma')
    print("Todas las palabras existen")
except:
    print("Error")
contador = 0
for sentence in generate(Gramatical,n=50):
    print(' '.join(sentence))
    contador+=1
Frase = ['el','hombre','toma','cafe']
parser = nltk.ChartParser(Gramatical)

for Arbol in parser.parse(Frase):
    print(Arbol)
print(contador)
